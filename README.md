# TopBites



## Para Backend (En la carpeta backend)

#### Intalar un etorno de desarrollo:
```
$ python3 -v venv venv
$ venv\Scripts\activate
```

#### Instalaciones:
```
$ pip install django
$ pip install mysqlclient
$ pip install django-cors-headers
```

#### Realizar las migraciones:
```
$ python manage.py makemigrations
$ python manage.py migrate
```

#### Correr el servidor:
```
$ python manage.py runserver
```

#### Configurar la base de datos:
En backend/backend/setting.py, hay que reemplazar los datos de acuerdo a su base de datos.

``` 
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'database_name',
        'USER': 'user_name',
        'PASSWORD': 'user_pass',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}
```


## Para Frontent (En la carpeta frontend)

#### Intalar dependencias:
```
$ npm i
```
#### Correr el servisor:
```
$ npm start
```

***

