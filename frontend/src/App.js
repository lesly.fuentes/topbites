import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import './App.css';
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import RestaurantTable from "./restaurants/RestaurantTable";

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" element={<RestaurantTable />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
