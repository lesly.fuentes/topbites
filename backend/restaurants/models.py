from django.db import models

class Restaurant (models.Model):
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    cusineType = models.CharField(max_length=100)
    rating = models.IntegerField()
    visited = models.BooleanField()

    def __str__(self):
        return self.name