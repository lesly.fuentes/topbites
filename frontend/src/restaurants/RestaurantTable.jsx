import React, { useEffect, useState } from "react";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Rating } from "primereact/rating";
import { getRestaurants, deleteRestaurant, getUnvisitedRestaurants } from "../services/RestaurantService";
import { Checkbox } from "primereact/checkbox";
import { Button } from "primereact/button";
import { ProgressSpinner } from "primereact/progressspinner";
import RestaurantForm from "./RestaurantForm";
import { InputSwitch } from "primereact/inputswitch";

const RestaurantTable = () => {
    const [restaurants, setRestaurants] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [showDialogRestaurantForm, setShowDialogRestaurantForm] = useState(false);
    const [headerDialogRestaurantForm, setHeaderDialogRestaurantForm] = useState();
    const [methodRestaurantForm, setMethodRestaurantForm] = useState();
    const [rowdataRestaurantForm, setRowdataRestaurantForm] = useState();
    const [checkFilterUnvisited, setCheckFilterUnvisited] = useState(false);

    const fetchData = async () => {
        const data = await getRestaurants();
        setRestaurants(data);
        setIsLoading(false);
    };

    const fetchDeleteRestaurant = async (id) => {
        const data = await deleteRestaurant(id);
        fetchData();
    }
    const fetchUnvisitedRestaurants = async () => {
        const data = await getUnvisitedRestaurants();
        setRestaurants(data);
        setIsLoading(false);
    }


    useEffect(() => {
        fetchData();
    }, []);

    const handleDeleteButton = (id) => {
        setIsLoading(true);
        fetchDeleteRestaurant(id);
    }

    const handleFilterButton = (e) => {
        setCheckFilterUnvisited(e.value);
        setIsLoading(true);
        if (e.value) fetchUnvisitedRestaurants();
        else fetchData();

    }

    const handleAddButton = () => {
        const initialValuesRowdata = {
            name: '',
            location: '',
            cusineType: '',
            rating: 0,
            visited: false
        }
        setHeaderDialogRestaurantForm("Agregar nuevo restaurante");
        setRowdataRestaurantForm(initialValuesRowdata);
        setMethodRestaurantForm("Add");
        setShowDialogRestaurantForm(true);
    }

    const handleEditButton = (rowdata) => {
        setHeaderDialogRestaurantForm("Editar");
        setMethodRestaurantForm("Edit")
        setRowdataRestaurantForm(rowdata);
        setShowDialogRestaurantForm(true);
    }


    const ratingBodyTemplate = (rowdata) => {
        return <Rating value={rowdata.rating} readOnly cancel={false} />
    }

    const visitedBodyTemplate = (rowdata) => {
        return <Checkbox checked={rowdata.visited} readOnly />
    }

    const editBodyTemplate = (rowdata) => {
        return <Button icon="pi pi-pencil" className="p-button-rounded" onClick={() => handleEditButton(rowdata)} />
    }

    const deleteBodyTemplate = (rowdata) => {
        return <Button icon="pi pi-times" className="p-button-rounded p-button-danger" onClick={() => handleDeleteButton(rowdata.id)} />
    }

    const renderHeader = () => {
        return (
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <h2 className="m-0">Restaurantes</h2>
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <div style={{ marginBottom: '2vh' }}>
                        <Button icon="pi pi-plus" label="Agregar restaurant" className="p-button-success" onClick={() => handleAddButton()} />
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <InputSwitch style={{ marginRight: '2vh' }} checked={checkFilterUnvisited} onChange={(e) => handleFilterButton(e)} />
                        <label>Mostrar los no visitados</label>
                    </div>
                </div>

            </div>
        )
    }

    const header = renderHeader();

    return (
        <div>
            <RestaurantForm headerDialog={headerDialogRestaurantForm} method={methodRestaurantForm} rowdata={rowdataRestaurantForm} showDialogRestaurantForm={showDialogRestaurantForm} setShowDialogRestaurantForm={setShowDialogRestaurantForm} setIsLoading={setIsLoading} setRestaurants={setRestaurants} />
            {isLoading ? <div style={{ position: 'fixed', top: 0, left: 0, width: '100%', height: '100%', backgroundColor: 'rgba(255, 255, 255, 0.5)', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <ProgressSpinner style={{ width: '50px', height: '50px' }} strokeWidth="8" fill="var(--surface-ground)" animationDuration=".5s" />
            </div> :
                <div style={{ margin: '5vh' }}>
                    <DataTable style={{ border: '1px solid rgba(0, 0, 0, 0.1)', padding: '20px' }} value={restaurants} paginator className="p-datatable-customers" header={header} rows={10}
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown" rowsPerPageOptions={[10, 25, 50]}
                        dataKey="id" rowHover
                        emptyMessage="No se encontraron restaurantes."
                        currentPageReportTemplate="Mostrar {first} a {last} de {totalRecords} resaturantes">
                        <Column field="name" sortable header="Name" style={{ minWidth: '14rem' }} />
                        <Column field="location" sortable header="Ubicación" style={{ minWidth: '14rem' }} />
                        <Column field="cusineType" sortable header="Tipo de comida" style={{ minWidth: '14rem' }} />
                        <Column field="rating" sortable header="Calificación" headerStyle={{ width: '4rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible' }} body={ratingBodyTemplate} />
                        <Column field="visited" sortable header="Visitado" headerStyle={{ width: '4rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible' }} body={visitedBodyTemplate} />
                        <Column header="Editar" headerStyle={{ width: '4rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible' }} body={editBodyTemplate} />
                        <Column header="Eliminar" headerStyle={{ width: '4rem', textAlign: 'center' }} bodyStyle={{ textAlign: 'center', overflow: 'visible' }} body={deleteBodyTemplate} />
                    </DataTable>
                </div>
            }
        </div>
    )
}
export default RestaurantTable;