import axios from 'axios';

const apiURL = 'http://127.0.0.1:8000/';


const getRestaurants = async () => {
    try {
        const response = await axios.get(`${apiURL}/restaurants/`);
        return response.data.body;
    } catch (error) {
        console.error(error);
    }
}

const getUnvisitedRestaurants = async () => {
    try {
        const response = await axios.get(`${apiURL}/restaurants/unvisited/`);
        return response.data.body;
    } catch (error) {
        console.error(error);
    }
}

const deleteRestaurant = async (id) => {
    try {
        const response = await axios.delete(`${apiURL}/restaurant/${id}/delete/`);
        return response.data;
    } catch (error) {
        console.error(error);
    }
}

const editRestaurant = async (id, body) => {
    try {
        const response = await axios.put(`${apiURL}/restaurant/${id}/update/`, body);
        return response.data;
    } catch (error) {
        console.error(error);
    }
}

const addRestaurant = async (body) => {
    try {
        const response = await axios.post(`${apiURL}/restaurant/create/`, body);
        return response.data;
    } catch (error) {
        console.error(error);
    }
}

export {
    getRestaurants,
    deleteRestaurant,
    editRestaurant,
    addRestaurant,
    getUnvisitedRestaurants
};