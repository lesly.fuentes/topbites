import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { useFormik } from 'formik';
import { InputText } from "primereact/inputtext";
import { Checkbox } from "primereact/checkbox";
import { Rating } from "primereact/rating";
import { classNames } from 'primereact/utils';
import { useEffect } from "react";
import { editRestaurant, getRestaurants, addRestaurant } from "../services/RestaurantService";

const RestaurantForm = ({ headerDialog, method, rowdata, showDialogRestaurantForm, setShowDialogRestaurantForm, setIsLoading, setRestaurants }) => {

    const fetchData = async () => {
        const data = await getRestaurants();
        setRestaurants(data);
        setIsLoading(false);
    };
    const handleVisitedChange = (e) => {
        const { checked } = e.target;
        formik.setFieldValue('visited', checked);
        if (!checked) {
            formik.setFieldValue('rating', 0);
        }
    };

    const handleRatingChange = (e) => {
        const { value } = e.target;
        if (value === null) formik.setFieldValue('rating', 0);
        else formik.setFieldValue('rating', value);

        if (value === null) formik.setFieldValue('visited', false);
        else formik.setFieldValue('visited', true);

    }

    const fetchEditRestaurant = async (id, body) => {
        const data = await editRestaurant(id, body);
        fetchData();
    }

    const fetchAddRestaurant = async (body) => {
        const data = await addRestaurant(body);
        fetchData();
    }


    const formik = useFormik({
        initialValues: {
            name: '',
            location: '',
            cusineType: '',
            rating: 0,
            visited: false
        },
        validate: (data) => {
            let errors = {};

            if (!data.name) {
                errors.name = 'Name is required.';
            }

            if (!data.location) {
                errors.location = 'Email is required.';
            }

            if (!data.cusineType) {
                errors.password = 'Password is required.';
            }

            return errors;
        },
        onSubmit: (data) => {
            setIsLoading(true);
            if (method === "Edit") fetchEditRestaurant(rowdata.id, data);
            if (method === "Add") fetchAddRestaurant(data);
        }
    });

    useEffect(() => {
        if (rowdata) formik.setValues(rowdata);
    }, [rowdata]);

    const isFormFieldValid = (name) => !!(formik.touched[name] && formik.errors[name]);
    const getFormErrorMessage = (name) => {
        return isFormFieldValid(name) && <small className="p-error">{formik.errors[name]}</small>;
    };

    return (
        <Dialog header={headerDialog} visible={showDialogRestaurantForm} onHide={() => setShowDialogRestaurantForm(false)}>
            <form onSubmit={formik.handleSubmit} className="p-fluid">
                <div className="field" style={{ margin: '2vh' }}>
                    <label htmlFor="name" className={classNames({ 'p-error': isFormFieldValid('name') })}>Nombre*</label>
                    <InputText id="name" name="name" value={formik.values.name} onChange={formik.handleChange} autoFocus className={classNames({ 'p-invalid': isFormFieldValid('name') })} />
                    {getFormErrorMessage('name')}
                </div>

                <div className="field" style={{ margin: '2vh' }}>
                    <label htmlFor="location" className={classNames({ 'p-error': isFormFieldValid('name') })}>Ubicación*</label>
                    <InputText id="location" name="location" value={formik.values.location} onChange={formik.handleChange} autoFocus className={classNames({ 'p-invalid': isFormFieldValid('location') })} />
                    {getFormErrorMessage('location')}
                </div>

                <div className="field" style={{ margin: '2vh' }}>
                    <label htmlFor="cusineType" className={classNames({ 'p-error': isFormFieldValid('cusineType') })}>Tipo de comida*</label>
                    <InputText id="cusineType" name="cusineType" value={formik.values.cusineType} onChange={formik.handleChange} autoFocus className={classNames({ 'p-invalid': isFormFieldValid('cusineType') })} />
                    {getFormErrorMessage('cusineType')}
                </div>

                <div className="field" style={{ margin: '2vh' }}>
                    <label htmlFor="rating" className={classNames({ 'p-error': isFormFieldValid('rating') })}>Calificación</label>
                    <Rating id='rating' name="rating" value={formik.values.rating} onChange={handleRatingChange} style={{ marginTop: '1vh' }} />
                </div>


                <div className="field-checkbox" style={{ margin: '2vh' }}>
                    <Checkbox style={{ marginRight: '2vh' }} inputId="visited" name="visited" checked={formik.values.visited} onChange={handleVisitedChange} className={classNames({ 'p-invalid': isFormFieldValid('visited') })} />
                    <label htmlFor="visited" className={classNames({ 'p-error': isFormFieldValid('visited') })}>Ya visité el restaurante</label>
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', margin: '2vh' }}>
                    <Button type="button" label="Cancelar" onClick={() => setShowDialogRestaurantForm(false)} icon="pi pi-times" className="p-button-text" />
                    <Button type="submit" label="Guardar" icon="pi pi-check" autoFocus />
                </div>
            </form>
        </Dialog>
    )
}

export default RestaurantForm;