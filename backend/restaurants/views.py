from django.http import JsonResponse
from .models import Restaurant
from .form import RestaurantForm
from django.views.decorators.csrf import csrf_exempt
import json


def get_restaurants(request):
    if request.method == 'GET':
        content = Restaurant.objects.all()
        data = [{'id':item.id,'name': item.name, 'location': item.location, 'cusineType': item.cusineType, 'rating': item.rating, 'visited': item.visited} for item in content]
        return JsonResponse({'body': data,'message':'All restaurants were successfully fetched.', 'status': 'success'})
    else:
        return JsonResponse({'message':'The request must be of type GET.', 'status': 'error'})
    

def get_unvisited_restaurants(request):
    if request.method == 'GET':
        content = Restaurant.objects.filter(visited=False)
        data = [{'id':item.id,'name': item.name, 'location': item.location, 'cusineType': item.cusineType, 'rating': item.rating, 'visited': item.visited} for item in content]
        return JsonResponse({'body': data,'message':'All restaurants were successfully fetched.', 'status': 'success'})
    else:
        return JsonResponse({'message':'The request must be of type GET.', 'status': 'error'})

@csrf_exempt
def create_restaurant(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        form = RestaurantForm(data)
        if form.is_valid():
            form.save()
            return JsonResponse({'message':'The restaurant was successfully created.', 'status': 'success'})
        else:
            errors = form.errors.as_json()
            return JsonResponse({'message':"The restaurant wasn't successfully created.", 'status': 'error', 'errors': errors})
    else:
        return JsonResponse({'message':'The request must be of type POST.', 'status': 'error'})

@csrf_exempt
def update_restaurant(request, id):
    try:
        restaurant = Restaurant.objects.get(pk=id)
    except Restaurant.DoesNotExist:
        return JsonResponse({'message': "Restaurant doesn't exist.", 'status': 'error'})

    if request.method == 'PUT':
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({'message': 'Invalid JSON format in request body.', 'status': 'error'})
        form = RestaurantForm(data, instance=restaurant)
        if form.is_valid():
            form.save()
            return JsonResponse({'message': 'The restaurant was successfully updated.', 'status': 'success'})
        else:
            errors = form.errors.as_json()
            return JsonResponse({'message': "The restaurant wasn't successfully updated.", 'status': 'error', 'errors': errors})
    else:
        return JsonResponse({'message': 'The request must be of type PUT.', 'status': 'error'})
    
@csrf_exempt
def delete_restaurant(request, id):
    try:
        restaurant = Restaurant.objects.get(pk=id)
    except Restaurant.DoesNotExist:
        return JsonResponse({'message': "Restaurant doesn't exist.", 'status': 'error'})

    if request.method == 'DELETE':
        restaurant.delete()
        return JsonResponse({'message':'The restaurant was successfully deleted.', 'status': 'success'})
    else:
        return JsonResponse({'message':'The request must be of type DELETE.', 'status': 'error'})

